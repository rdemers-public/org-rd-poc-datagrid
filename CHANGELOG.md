# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.0.RELEASE] - 2018-07-08
### Added
- Distributed cache with Spring integration.

### Changed
- Update to Spring/Boot 2.0.3.RELEASE

### Fixed
- English translation.

### Removed
- None.

## [1.0.0.SNAPSHOT] - 2018-06-01
### Added
- POC about datagrid with Spring/Boot and Hazelcast.

### Changed
- None.

### Removed
- None.

[1.0.0.RELEASE]: https://gitlab.com/rdemers-public/org-rd-poc-datagrid/tree/master
[1.1.0-SNAPSHOT]: https://gitlab.com/rdemers-public/org-rd-poc-datagrid/tree/develop
