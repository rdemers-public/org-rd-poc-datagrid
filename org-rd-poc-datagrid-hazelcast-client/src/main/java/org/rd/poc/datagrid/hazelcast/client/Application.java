package org.rd.poc.datagrid.hazelcast.client;
/*
 * Copyright 2018 Réal Demers; email: real.demers@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import java.util.Iterator;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.CacheManager;

import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.ILock;
import com.hazelcast.core.Member;

@SpringBootApplication
public class Application implements CommandLineRunner
{
    private static Logger log = LoggerFactory.getLogger(Application.class);

    @Autowired
    ClientConfig clientConfig;

    @Autowired
    private CacheManager cacheManager;

    @Autowired
    private CacheService cacheService;

    @Autowired
    private HazelcastInstance hazelcastInstance;

    public static void main(String[] args) throws Exception
    {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception
    {
        log.info("Setup configuration         : Spring Boot/Hazelcast");
        log.info("- Config. client Hazelcast  : " + clientConfig.toString());
        log.info("- Hazelcast(client) instance: " + hazelcastInstance.getClass().getName());

        log.info("- Cache manager: " + cacheManager.getClass().getName());
        log.info("- Cache service: " + cacheService.getClass().getName());

        Member member;
        Set<Member> memberSet = hazelcastInstance.getCluster().getMembers();
        log.info("Cluster/Members:");
        for (Iterator<Member> memberIter = memberSet.iterator(); memberIter.hasNext();)
        {
            member = memberIter.next();
            log.info("- Member: " + member.getUuid() + ", " + member.getAddress());
        }

        // cacheService.clearCache();

        // Cache tests.
        checkEntry("Jack the snake Robert");
        checkEntry("Mad Dog Vachon");
        checkEntry("Jack the snake Robert");
        checkEntry("Mad Dog Vachon");
        checkEntry("Ultimate Warrior");
        checkEntry("Ultimate Warrior");

        // Lock Tests.
        lock("12345");
        lock("12345");
        lock("12345");
        lock("12346");
        lock("12346");
        lock("12346");
        lock("12347");
        lock("12347");
        lock("12347");
        lock("12348");
        lock("12348");
        lock("12348");
        lock("12349");
        lock("12349");
        lock("12349");

        hazelcastInstance.shutdown();

        // SpringBoot register a "hook" with the JVM to control the shutdown process.
        log.info("Execution completed !");
        System.exit(0);
    }

    private void checkEntry(String entry)
    {
        log.info("Call cache service: " + CacheService.class.getSimpleName() + " " + entry);
        log.info("return: " + cacheService.getCacheEntry(entry));
    }

    private void lock(String lockName)
    {
        ILock locker = hazelcastInstance.getLock(lockName+".lock");
        try
        {
            log.info("I want a lock for: " + lockName);
            if (locker.tryLock(1500, TimeUnit.MILLISECONDS, 5000, TimeUnit.MILLISECONDS))
            {
                // Job simulation.
                log.info("I got a lock on: " + lockName);
                int sleepTime = new Random().nextInt(4000-1000) + 1000;
                Thread.sleep(sleepTime); // Between 1 and 4 seconds.
            }
            else
                log.info("I didn't get a lock (inside 1.5 seconds) for: " + lockName);

        } 
        catch (InterruptedException ex)
        {
            log.info("Exception: " + ex.getMessage());
        }
        finally
        {
            if (locker.isLockedByCurrentThread()) 
            { 
                log.info("I unlock: " + locker.toString());
                try { locker.unlock(); } catch (Exception ex) {}; // milliseconds lease time possibility.
            }
        }
    }
}