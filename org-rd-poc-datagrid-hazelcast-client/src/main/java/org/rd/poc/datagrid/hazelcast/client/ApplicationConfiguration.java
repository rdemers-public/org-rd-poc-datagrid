package org.rd.poc.datagrid.hazelcast.client;
/*
 * Copyright 2018 Réal Demers; email: real.demers@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.cache.interceptor.SimpleKeyGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.spring.cache.HazelcastCacheManager;

@Configuration
@EnableCaching
public class ApplicationConfiguration
{
    @Bean
    public ClientConfig  hazelCastClientConfig()
    {
        ClientConfig cfg = new ClientConfig();

        cfg.getGroupConfig()
            .setName("datagrid")
            .setPassword("datagrid");

        cfg.getNetworkConfig()
            .setConnectionAttemptLimit(5)
            .setConnectionTimeout(5000)
            .setConnectionAttemptPeriod(5000)
            .addAddress("localhost:5900", "localhost:5901")
            .getSocketOptions()
                .setBufferSize(32)
                .setKeepAlive(true)
                .setTcpNoDelay(true)
                .setReuseAddress(true)
                .setLingerSeconds(3);

        return cfg;
    }

    @Bean
    HazelcastInstance getHazelcastInstance()
    {
    	return HazelcastClient.newHazelcastClient(hazelCastClientConfig());
    }

    @Bean
    KeyGenerator keyGenerator()
    {
        return new SimpleKeyGenerator();
    }

    @Bean
    CacheManager cacheManager()
    {
        return new HazelcastCacheManager(getHazelcastInstance());
    }
}