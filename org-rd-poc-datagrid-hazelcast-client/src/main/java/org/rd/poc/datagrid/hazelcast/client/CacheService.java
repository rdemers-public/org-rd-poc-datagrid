package org.rd.poc.datagrid.hazelcast.client;
/*
 * Copyright 2018 Réal Demers; email: real.demers@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.*;
import org.springframework.stereotype.Service;

@Service
@CacheConfig(cacheNames = "org.rd.poc.datagrid.wwf") // WWF - World's Wresting Federation.
public class CacheService
{
    private static Logger log = LoggerFactory.getLogger(CacheService.class);

    @CacheEvict(allEntries = true)
    public void clearCache() { }

    @Cacheable
    public String getCacheEntry(String entry)
    {
        log.info("*** Executing: " + entry);
        return "An entry: " + entry;
    }
}