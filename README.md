# ORG.RD.POC.DATAGRID

POC about using Hazelcast for a distributed datagrid with Spring/Boot.

## Getting Started

```
git clone git@gitlab.com:rdemers-public/org-rd-poc-datagrid.git
```

### Prerequisites

What things you need to install the software and how to install them

```
Git 2.6.X
JDK/Java 1.8.x
Apache/Maven 3.3.x
IDE of your choice (ex.: Eclipse)
```

### Installing

A step by step series of examples that tell you how to get a development env running
Say what the step will be

```
POC only.
```

## Running the tests

```
POC only.
```

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
POC only.
```

### And coding style tests

Explain what these tests test and why

```
POC only.
```

## Deployment

```
POC only.
```

## Built With

* [Eclipse](http://www.eclipse.org/) - IDE
* [Maven](https://maven.apache.org/) - Dependency Management

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/rdemers-public/org-rd-poc-datagrid/tags). 

## Authors

* **Réal Demers** - *Initial work* - [Gitlab profil](https://gitlab.com/RDemers)

## License

This project is licensed under the APACHE License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Forks at gitlab.

## References
[Spring/Boot - Hazelcast](https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-hazelcast.html)