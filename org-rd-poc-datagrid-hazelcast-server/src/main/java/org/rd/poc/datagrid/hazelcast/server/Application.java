package org.rd.poc.datagrid.hazelcast.server;
/*
 * Copyright 2018 Réal Demers; email: real.demers@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import java.util.Iterator;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.Member;

@SpringBootApplication
public class Application implements CommandLineRunner
{
    private static Logger log = LoggerFactory.getLogger(Application.class);

    @Autowired
    private HazelcastInstance  hazelcastInstance;

    public static void main(String[] args) throws Exception
    {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception
    {
        log.info("Setup configuration : Spring Boot/Hazelcast");
        log.info("- Hazelcast instance: " + hazelcastInstance.getClass().getName());

        Member member;
        Set<Member> memberSet = hazelcastInstance.getCluster().getMembers();
        log.info("Cluster/Members:");
        for (Iterator<Member> memberIter = memberSet.iterator(); memberIter.hasNext();)
        {
            member = memberIter.next();
            log.info("- Member: " + member.getUuid() + ", " + member.getAddress());
        }
    }
}
