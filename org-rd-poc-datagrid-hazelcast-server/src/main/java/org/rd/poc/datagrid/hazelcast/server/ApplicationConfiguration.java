package org.rd.poc.datagrid.hazelcast.server;
/*
 * Copyright 2018 Réal Demers; email: real.demers@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.cache.interceptor.SimpleKeyGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.hazelcast.config.Config;
import com.hazelcast.config.EntryListenerConfig;
import com.hazelcast.config.EvictionPolicy;
import com.hazelcast.config.MapConfig;
import com.hazelcast.config.MaxSizeConfig;

@Configuration
public class ApplicationConfiguration
{
	@Bean
    public Config hazelCastConfig()
    {
        Config cfg = new Config();

		cfg.setInstanceName("hazelcast-instance")
            .addMapConfig
            (
                new MapConfig()
                    .setName("org.rd.poc.datagrid.wwf")
                    .addEntryListenerConfig(new EntryListenerConfig(new MapEntryListener(),true,true))
                    .setMaxSizeConfig(new MaxSizeConfig(200, MaxSizeConfig.MaxSizePolicy.FREE_HEAP_SIZE))
                    .setEvictionPolicy(EvictionPolicy.NONE)
                    .setTimeToLiveSeconds(12000)
            );

        // À ajuster ... Bon pour un POC seulement.
        cfg.getNetworkConfig()
            .setPort(5900)
            .setPortAutoIncrement(false)
            .getJoin()
               .getMulticastConfig()
                   .setEnabled(false);

        cfg.getNetworkConfig()
            .setPort(5900)
            .setPortAutoIncrement(true)
            .getJoin()
               .getTcpIpConfig()
                   .addMember("localhost")
                   .setRequiredMember(null)
                   .setEnabled(true);

        cfg.getGroupConfig().setName("datagrid").setPassword("datagrid");
        //cfg.setProperty("hazelcast.initial.min.cluster.size", "2");
        return cfg;
    }

    @Bean
    KeyGenerator keyGenerator()
    {
        return new SimpleKeyGenerator();
    }
}